package com.softtek.academy.javaweb.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.softtek.academy.javaweb.beans.BeanData;

public class LoginDAO {
	
	//Función que obtiene la conexión para operar la base de datos
	public static Connection getConnection() {
		//Definimos los parámetros para acceder a la DB
		final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		final String DB_URL = "jdbc:mysql://localhost:3306/nuevoschema?"
				+ "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		final String USER = "root";
		final String PASS = "1234";
		Connection con = null;
		
		try {
			Class.forName(JDBC_DRIVER);
			con = DriverManager.getConnection(DB_URL, USER, PASS);
		}catch(Exception e){
			System.out.println(e);
		}
		
		return con;
	}
	
	public static ArrayList<BeanData> getActividadesPendientes() {
		ArrayList <BeanData> lista = new ArrayList<BeanData>();
		
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM TO_DO_LIST WHERE is_done = 0;");
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				//Agregamos los nuevos datos a la lista
				lista.add( new BeanData(rs.getInt("id"), rs.getString("list")) );
			}
		}catch(Exception e) {
			System.out.print(e);
			return lista;
		}
		return lista;
	}
	
	public static ArrayList<BeanData> getActividadesTerminadas() {
		ArrayList <BeanData> lista = new ArrayList<BeanData>();
		
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM TO_DO_LIST WHERE is_done = 1;");
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				//Agregamos los nuevos datos a la lista
				lista.add( new BeanData(rs.getInt("id"), rs.getString("list")) );
			}
		}catch(Exception e) {
			System.out.print(e);
			return lista;
		}
		return lista;
	}
	
	public static void ingresarActividad(String actividad) {
		try {
			String query = "INSERT INTO TO_DO_LIST (list) values ('"+ actividad +"');";
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement(query);
			System.out.println("Preparando Insert 2.0...");
			ps.execute();
			
		} catch (Exception e) { System.out.print(e); }
	}
	
	public static void terminarActividad(int id) {
		try {
			String query = "UPDATE TO_DO_LIST SET is_done = 1 WHERE id = " + id + ";";
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement(query);
			System.out.println("Actualizando id -> " + id);
			ps.execute();
		}catch (Exception e) { System.out.print(e);}
	}

}
