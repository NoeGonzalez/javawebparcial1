package com.softtek.academy.javaweb.beans;

import com.softtek.academy.javaweb.dao.LoginDAO;

public class BeanData {
	private int id;
	private String list;
	private boolean is_done;
	
	public BeanData() {
		super();
	}
	
	
	
	public BeanData(String list) {
		super();
		this.list = list;
	}



	public BeanData(int id, String list) {
		super();
		this.id = id;
		this.list = list;
	}
	
	public BeanData(int id, String list, boolean is_done) {
		super();
		this.id = id;
		this.list = list;
		this.is_done = is_done;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getList() {
		return list;
	}
	public void setList(String list) {
		this.list = list;
	}
	public boolean isIs_done() {
		return is_done;
	}
	public void setIs_done(boolean is_done) {
		this.is_done = is_done;
	}
	
	public void insertarActividad() {
		System.out.println("Se inici� la inserci�n de la actividad");
		
		System.out.println("Datos en el bean:");
		System.out.println("Actividad = " + list);
		
		LoginDAO.ingresarActividad(list);
		
		System.out.println("...\n Se termin� la inserci�n de la actividad");
	}
}
