package com.softtek.academy.javaweb.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.beans.BeanData;
import com.softtek.academy.javaweb.dao.LoginDAO;

/**
 * Servlet implementation class ActualizarActividad
 */
public class ActualizarActividad extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ActualizarActividad() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		String docType = "<!doctype html public \"-//w3c//dtd html 4.0" + "transitional//en\">\n";
		PrintWriter out = response.getWriter();
		
		int id = Integer.parseInt(request.getParameter("id"));
		String contextPath = request.getParameter("contextPath");
		
		System.out.println("Se est�n cambiando datos del id -> " + id);
		LoginDAO.terminarActividad(id);
		
		ArrayList <BeanData> actividadesTerminadas = new ArrayList <BeanData>();
		actividadesTerminadas = LoginDAO.getActividadesTerminadas();
		
		if(actividadesTerminadas != null) {
			request.setAttribute("result", actividadesTerminadas);
		}
		else {
			request.setAttribute("result", null);
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("views/ActividadesTerminadas.jsp");
		rd.forward(request, response);
		
		
	}

}
