package com.softtek.academy.javaweb.controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.beans.BeanData;

/**
 * Servlet implementation class SubirActividad
 */
public class SubirActividad extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubirActividad() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		String docType = "<!doctype html public \"-//w3c//dtd html 4.0" + "transitional//en\">\n";
		PrintWriter out = response.getWriter();
		
		String actividad = request.getParameter("actividad");
		System.out.println("Se recibieron datos -> " + actividad);
		BeanData actividadBean = new BeanData(actividad);
		actividadBean.insertarActividad();
		
		RequestDispatcher rd = request.getRequestDispatcher("views/Form_AgregarActividad.jsp");
		rd.forward(request, response);
		
		
		
		
	}

}
