<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%  String contextPath = request.getContextPath(); %>    

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<table>
		<tr>
			<th>Numero de Tarea:</th>
			<th>Descripción</th>
		</tr>
		<c:forEach var="list" items="${result}">
           <tr>
               <td><c:out value = "${list.getId()}"/></td>
            <td><c:out value = "${list.getList()}"/></td>
            <td>
	             <form action="<%=contextPath%>/ServletController" method="post">
	                 <button type="submit" >Done</button>
	                 <input type="hidden" name="option" value="ListDone">
	                 <input type="hidden" name="id" value="${list.getId()}">
	             </form>
            </td>
        </tr>
	  </c:forEach>
	</table>
	
	<div><hr></div>
	
	<form action="<%= contextPath %>/views/Form_AgregarActividad.jsp" method="post">
	  <input type="submit" value="Agregar Actividad a la lista"> <br>
	</form>
	
	<form action="/JavaWeb_ExamenParcial1/VerActividadesPendientes" method="post">
	  <input type="submit" value="Ver actividades pendientes">
	</form>
</body>
</html>